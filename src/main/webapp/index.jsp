<%-- 
    Document   : index
    Created on : 17-12-2019, 16:31:24
    Author     : Gonza
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>GUI - CRUD Mantenedor - Gonzalo Meneses</title>
    </head>
    <body>
        <h2>Seleccione una de las acciones que desea realizar:</h2><br>
        <form action="Graba" method="post">
            ID Proveedor: <input type="text" name="idpro"/><br><br>
            Rut Proveedor: <input type="text" name="rutpro"/><br><br>
            Nombre Proveedor: <input type="text" name="nompro"/><br><br>
            Razon Social: <input type="text" name="razonpro"/><br><br>
            Rut Representante: <input type="text" name="rutrep"/><br><br>
            Nombre Representante: <input type="text" name="nomrep"/><br><br>
            <input type="submit" value="Grabar"/><br><br>
        </form><br>
        <a>------------------------------------------------</a><br>
        <a href="views/listar.jsp">Listar Proveedores</a><br><br>
        <a href="views/about.jsp">Sobre Mi</a>
    </body>
</html>
