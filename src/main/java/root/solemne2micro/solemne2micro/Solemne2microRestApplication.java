package root.solemne2micro.solemne2micro;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("api")
@ApplicationScoped
public class Solemne2microRestApplication extends Application {
}
