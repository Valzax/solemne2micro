/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.solemne2micro.solemne2micro;

import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gonza
 */
@WebServlet(name = "Graba", urlPatterns = {"/Graba"})
public class Graba extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("idpro");
        String nombre = request.getParameter("nompro");
        String rut = request.getParameter("rutpro");
        String razon = request.getParameter("razonpro");
        String rutrepre = request.getParameter("rutrep");
        String nomrepre = request.getParameter("nomrep");
        root.persistence.entities.ProProveedores proveedores = new root.persistence.entities.ProProveedores();
        proveedores.setProIdproveedor(id);
        proveedores.setProNomproveedor(nombre);
        proveedores.setProRutproovedor(rut);
        proveedores.setProRazonsocial(razon);
        proveedores.setProRutrepresentante(rutrepre);
        proveedores.setProRepresentante(nomrepre);
        EntityManager em;
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("ProveedoresMicro_PU");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(proveedores);
        em.flush();
        em.getTransaction().commit();
        
        response.sendRedirect("views/correcto.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
